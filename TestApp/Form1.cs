﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DirectoryInfo startDirectory;
        int CheckedFilesCount;
        int FindFilesCount;
        CancellationTokenSource cts;
        DateTime time;

        private void SetDirButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogresult = DirBrowserDialog.ShowDialog();
            DirBrowserDialog.Description = "Выбор папки";
            string folder = "";
            if (dialogresult == DialogResult.OK)
            {
                folder = DirBrowserDialog.SelectedPath ;
                PathToDirLabel.Text = "Стартовая директория: " + folder;
                startDirectory = new DirectoryInfo(folder + "\\");
            }

        }

        private async void SearchButton_Click(object sender, EventArgs e)
        {

            if (startDirectory == null) { MessageBox.Show("Путь к папке не задан."); return; };
            if (!startDirectory.Exists) { MessageBox.Show("Указанной папки не существует."); return; };
            if (TempNameTextBox.Text == "") { MessageBox.Show("Маска названия файла не задана. \nДля выбора всех файлов введите в поле \"*\""); return; };
            if (FindTextBox.Text == "") { MessageBox.Show("Текст для поиска не задан."); return; };

            CheckedFilesCount = 0;
            FindFilesCount = 0;
            time = new DateTime();
            TimeLabel.Text = time.ToLongTimeString();
            NumFindCountLabel.Text = "0";
            NumCheckedCountLabel.Text = "0";
            treeView.Nodes.Clear();
            treeView.Nodes.Add(startDirectory.Name);

            StopButton.Enabled = true;
            SearchButton.Enabled = false;
            SetDirButton.Enabled = false;
            string mask = TempNameTextBox.Text;
            string text = FindTextBox.Text;

            mask = mask.Replace(".", @"\.");
            mask = mask.Replace("?", ".");
            mask = mask.Replace("*", ".*");
            mask = "^" + mask + "$";

            Regex regMask = new Regex(mask, RegexOptions.IgnoreCase);
            text = Regex.Escape(text);
            Regex regText = new Regex(text, RegexOptions.IgnoreCase);
            Settings Set = new Settings(startDirectory, regMask, regText);

            cts = new CancellationTokenSource();

            timerSearch.Start();
            await Search(Set, cts.Token);
            timerSearch.Stop();
            if (!cts.IsCancellationRequested) { MessageBox.Show("Поиск успешно завершен.\r\n\r\nПроверено файлов: " + CheckedFilesCount + "\r\nНайдено подходящих файлов: " + FindFilesCount + "\r\nПрошло времени: " + time.ToLongTimeString()); }
            else { MessageBox.Show("Поиск остановлен."); };
            StopButton.Enabled = false;
            SearchButton.Enabled = true;
            SetDirButton.Enabled = true;
        }

        Task Search(Settings Set, CancellationToken Token)
        {
            Task T = new Task(() => FindTextInFiles(Set, Token));
            T.Start();
            return T;
        }


        void FindTextInFiles(Settings Set, CancellationToken Token)
        {
            try
            {
                Token.ThrowIfCancellationRequested();

                StreamReader sr = null;
                MatchCollection mc = null;
                int CountOfMatchFiles = 0;

                FileInfo[] fi = null;
                try
                {
                    fi = Set.dir.GetFiles();
                }
                catch
                {
                    return;
                }

                foreach (FileInfo f in fi)
                {
                    Token.ThrowIfCancellationRequested();

                    if (Set.regMask.IsMatch(f.Name))
                    {
                        ++CountOfMatchFiles;
                        IncCheckCount();
                        ShowCurFileName(f.Name);
                        try
                        {
                            sr = new StreamReader(Set.dir.FullName + @"\" + f.Name, Encoding.Default);
                        }
                        catch
                        {
                            continue;
                        }

                        string Content = sr.ReadToEnd();
                        sr.Close();

                        if (Set.regText.IsMatch(Content))
                        {
                            IncFindCount();
                            AddTreeNode(Set.dir.FullName + "\\" + f.Name);
                        };

                    }
                    Token.ThrowIfCancellationRequested();
                }

                DirectoryInfo[] diSub = Set.dir.GetDirectories();

                foreach (DirectoryInfo diSubDir in diSub) FindTextInFiles(new Settings(diSubDir, Set.regMask, Set.regText), Token);

                Token.ThrowIfCancellationRequested();
            }
            catch (OperationCanceledException ae)
            {
                return;
            }
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            cts.Cancel();
        }

        private void timerSearch_Tick(object sender, EventArgs e)
        {
            time = time.AddSeconds(1);
            TimeLabel.Text = time.ToLongTimeString();
        }

        void IncCheckCount()
        {
            NumCheckedCountLabel.Invoke((MethodInvoker)(delegate() { NumCheckedCountLabel.Text = (++CheckedFilesCount).ToString(); }));
        }

        void IncFindCount()
        {
            NumFindCountLabel.Invoke((MethodInvoker)(delegate() { NumFindCountLabel.Text = (++FindFilesCount).ToString(); }));
        }
        void ShowCurFileName(string FileName)
        {
            CurFileNameLabel.Invoke((MethodInvoker)(delegate() { CurFileNameLabel.Text = FileName; }));
        }

        void AddTreeNode(string Path)
        {
            Path = Path.Remove(0, startDirectory.FullName.Length);
            string[] folders;
            if (Path[0] == '\\')
            { Path = Path.Replace("\\", ""); treeView.Invoke((MethodInvoker)(delegate() { treeView.Nodes[0].Nodes.Add(Path); })); }
            else
            {
                folders = Path.Split('\\');
                TreeNode node = treeView.Nodes[0];
                for (int i = 0; i < folders.Length - 1; i++)
                {

                    if (node.Nodes.ContainsKey(folders[i]) == true) { node = node.Nodes.Find(folders[i], false)[0]; }
                    else { treeView.Invoke((MethodInvoker)(delegate() { node = node.Nodes.Add(folders[i]); node.Name = folders[i]; })); }
                }
                treeView.Invoke((MethodInvoker)(delegate() { node.Nodes.Add(folders[folders.Length - 1]); }));
            }
            ExpandCheckBox.Invoke((MethodInvoker)(delegate() { if (ExpandCheckBox.Checked) { treeView.ExpandAll(); } }));
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.TempName = TempNameTextBox.Text;
            Properties.Settings.Default.FindText = FindTextBox.Text;
            Properties.Settings.Default.Path = startDirectory.FullName;
            Properties.Settings.Default.Save();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TempNameTextBox.Text = Properties.Settings.Default.TempName;
            FindTextBox.Text = Properties.Settings.Default.FindText;
            string folder = Properties.Settings.Default.Path;
            DirectoryInfo Dir = new DirectoryInfo(folder);
            if (Dir.Exists) { PathToDirLabel.Text = "Стартовая директория: " + folder; startDirectory = Dir; }           
        }
    }
}
