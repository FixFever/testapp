﻿namespace TestApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DirBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.SetDirButton = new System.Windows.Forms.Button();
            this.PathToDirLabel = new System.Windows.Forms.Label();
            this.TempNameLabel = new System.Windows.Forms.Label();
            this.TempNameTextBox = new System.Windows.Forms.TextBox();
            this.FindTextLabel = new System.Windows.Forms.Label();
            this.FindTextBox = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.CurFileLabel = new System.Windows.Forms.Label();
            this.CurFileNameLabel = new System.Windows.Forms.Label();
            this.CheckedCountLabel = new System.Windows.Forms.Label();
            this.NumCheckedCountLabel = new System.Windows.Forms.Label();
            this.SearchTimeLabel = new System.Windows.Forms.Label();
            this.TimeLabel = new System.Windows.Forms.Label();
            this.StopButton = new System.Windows.Forms.Button();
            this.timerSearch = new System.Windows.Forms.Timer(this.components);
            this.treeView = new System.Windows.Forms.TreeView();
            this.criteriaGroupBox = new System.Windows.Forms.GroupBox();
            this.FindCountLabel = new System.Windows.Forms.Label();
            this.NumFindCountLabel = new System.Windows.Forms.Label();
            this.TreeViewLabel = new System.Windows.Forms.Label();
            this.ExpandCheckBox = new System.Windows.Forms.CheckBox();
            this.SearchGroupBox = new System.Windows.Forms.GroupBox();
            this.criteriaGroupBox.SuspendLayout();
            this.SearchGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SetDirButton
            // 
            this.SetDirButton.Location = new System.Drawing.Point(24, 120);
            this.SetDirButton.Name = "SetDirButton";
            this.SetDirButton.Size = new System.Drawing.Size(75, 23);
            this.SetDirButton.TabIndex = 0;
            this.SetDirButton.Text = "Обзор";
            this.SetDirButton.UseVisualStyleBackColor = true;
            this.SetDirButton.Click += new System.EventHandler(this.SetDirButton_Click);
            // 
            // PathToDirLabel
            // 
            this.PathToDirLabel.AutoEllipsis = true;
            this.PathToDirLabel.AutoSize = true;
            this.PathToDirLabel.Location = new System.Drawing.Point(109, 125);
            this.PathToDirLabel.MaximumSize = new System.Drawing.Size(300, 0);
            this.PathToDirLabel.Name = "PathToDirLabel";
            this.PathToDirLabel.Size = new System.Drawing.Size(121, 13);
            this.PathToDirLabel.TabIndex = 1;
            this.PathToDirLabel.Text = "Путь к папке не задан";
            // 
            // TempNameLabel
            // 
            this.TempNameLabel.AutoSize = true;
            this.TempNameLabel.Location = new System.Drawing.Point(21, 39);
            this.TempNameLabel.Name = "TempNameLabel";
            this.TempNameLabel.Size = new System.Drawing.Size(46, 13);
            this.TempNameLabel.TabIndex = 2;
            this.TempNameLabel.Text = "Маска: ";
            // 
            // TempNameTextBox
            // 
            this.TempNameTextBox.Location = new System.Drawing.Point(73, 36);
            this.TempNameTextBox.Name = "TempNameTextBox";
            this.TempNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.TempNameTextBox.TabIndex = 3;
            // 
            // FindTextLabel
            // 
            this.FindTextLabel.AutoSize = true;
            this.FindTextLabel.Location = new System.Drawing.Point(21, 79);
            this.FindTextLabel.Name = "FindTextLabel";
            this.FindTextLabel.Size = new System.Drawing.Size(103, 13);
            this.FindTextLabel.TabIndex = 4;
            this.FindTextLabel.Text = "Текст для поиска: ";
            // 
            // FindTextBox
            // 
            this.FindTextBox.Location = new System.Drawing.Point(130, 76);
            this.FindTextBox.Name = "FindTextBox";
            this.FindTextBox.Size = new System.Drawing.Size(100, 20);
            this.FindTextBox.TabIndex = 5;
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(13, 163);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(75, 23);
            this.SearchButton.TabIndex = 6;
            this.SearchButton.Text = "Старт";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // CurFileLabel
            // 
            this.CurFileLabel.AutoSize = true;
            this.CurFileLabel.Location = new System.Drawing.Point(27, 240);
            this.CurFileLabel.Name = "CurFileLabel";
            this.CurFileLabel.Size = new System.Drawing.Size(114, 13);
            this.CurFileLabel.TabIndex = 8;
            this.CurFileLabel.Text = "Проверяемый файл: ";
            // 
            // CurFileNameLabel
            // 
            this.CurFileNameLabel.AutoSize = true;
            this.CurFileNameLabel.Location = new System.Drawing.Point(147, 240);
            this.CurFileNameLabel.MaximumSize = new System.Drawing.Size(300, 0);
            this.CurFileNameLabel.Name = "CurFileNameLabel";
            this.CurFileNameLabel.Size = new System.Drawing.Size(10, 13);
            this.CurFileNameLabel.TabIndex = 9;
            this.CurFileNameLabel.Text = "-";
            // 
            // CheckedCountLabel
            // 
            this.CheckedCountLabel.AutoSize = true;
            this.CheckedCountLabel.Location = new System.Drawing.Point(15, 71);
            this.CheckedCountLabel.Name = "CheckedCountLabel";
            this.CheckedCountLabel.Size = new System.Drawing.Size(138, 13);
            this.CheckedCountLabel.TabIndex = 10;
            this.CheckedCountLabel.Text = "Всего проверено файлов:";
            // 
            // NumCheckedCountLabel
            // 
            this.NumCheckedCountLabel.AutoSize = true;
            this.NumCheckedCountLabel.Location = new System.Drawing.Point(171, 275);
            this.NumCheckedCountLabel.Name = "NumCheckedCountLabel";
            this.NumCheckedCountLabel.Size = new System.Drawing.Size(10, 13);
            this.NumCheckedCountLabel.TabIndex = 11;
            this.NumCheckedCountLabel.Text = "-";
            // 
            // SearchTimeLabel
            // 
            this.SearchTimeLabel.AutoSize = true;
            this.SearchTimeLabel.Location = new System.Drawing.Point(27, 338);
            this.SearchTimeLabel.Name = "SearchTimeLabel";
            this.SearchTimeLabel.Size = new System.Drawing.Size(97, 13);
            this.SearchTimeLabel.TabIndex = 12;
            this.SearchTimeLabel.Text = "Прошло времени:";
            // 
            // TimeLabel
            // 
            this.TimeLabel.AutoSize = true;
            this.TimeLabel.Location = new System.Drawing.Point(130, 338);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(10, 13);
            this.TimeLabel.TabIndex = 13;
            this.TimeLabel.Text = "-";
            // 
            // StopButton
            // 
            this.StopButton.Enabled = false;
            this.StopButton.Location = new System.Drawing.Point(116, 163);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(75, 23);
            this.StopButton.TabIndex = 14;
            this.StopButton.Text = "Стоп";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // timerSearch
            // 
            this.timerSearch.Interval = 1000;
            this.timerSearch.Tick += new System.EventHandler(this.timerSearch_Tick);
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(432, 45);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(273, 345);
            this.treeView.TabIndex = 15;
            // 
            // criteriaGroupBox
            // 
            this.criteriaGroupBox.Controls.Add(this.SetDirButton);
            this.criteriaGroupBox.Controls.Add(this.PathToDirLabel);
            this.criteriaGroupBox.Controls.Add(this.TempNameTextBox);
            this.criteriaGroupBox.Controls.Add(this.TempNameLabel);
            this.criteriaGroupBox.Controls.Add(this.FindTextLabel);
            this.criteriaGroupBox.Controls.Add(this.FindTextBox);
            this.criteriaGroupBox.Location = new System.Drawing.Point(12, 18);
            this.criteriaGroupBox.Name = "criteriaGroupBox";
            this.criteriaGroupBox.Size = new System.Drawing.Size(400, 173);
            this.criteriaGroupBox.TabIndex = 16;
            this.criteriaGroupBox.TabStop = false;
            this.criteriaGroupBox.Text = "Критерии поиска";
            // 
            // FindCountLabel
            // 
            this.FindCountLabel.AutoSize = true;
            this.FindCountLabel.Location = new System.Drawing.Point(27, 307);
            this.FindCountLabel.Name = "FindCountLabel";
            this.FindCountLabel.Size = new System.Drawing.Size(95, 13);
            this.FindCountLabel.TabIndex = 17;
            this.FindCountLabel.Text = "Найдено файлов:";
            // 
            // NumFindCountLabel
            // 
            this.NumFindCountLabel.AutoSize = true;
            this.NumFindCountLabel.Location = new System.Drawing.Point(130, 307);
            this.NumFindCountLabel.Name = "NumFindCountLabel";
            this.NumFindCountLabel.Size = new System.Drawing.Size(10, 13);
            this.NumFindCountLabel.TabIndex = 18;
            this.NumFindCountLabel.Text = "-";
            // 
            // TreeViewLabel
            // 
            this.TreeViewLabel.AutoSize = true;
            this.TreeViewLabel.Location = new System.Drawing.Point(429, 18);
            this.TreeViewLabel.Name = "TreeViewLabel";
            this.TreeViewLabel.Size = new System.Drawing.Size(148, 13);
            this.TreeViewLabel.TabIndex = 19;
            this.TreeViewLabel.Text = "Дерево найденных файлов:";
            // 
            // ExpandCheckBox
            // 
            this.ExpandCheckBox.AutoSize = true;
            this.ExpandCheckBox.Checked = true;
            this.ExpandCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ExpandCheckBox.Location = new System.Drawing.Point(432, 398);
            this.ExpandCheckBox.Name = "ExpandCheckBox";
            this.ExpandCheckBox.Size = new System.Drawing.Size(121, 17);
            this.ExpandCheckBox.TabIndex = 20;
            this.ExpandCheckBox.Text = "Раскрывать папки";
            this.ExpandCheckBox.UseVisualStyleBackColor = true;
            // 
            // SearchGroupBox
            // 
            this.SearchGroupBox.Controls.Add(this.SearchButton);
            this.SearchGroupBox.Controls.Add(this.StopButton);
            this.SearchGroupBox.Controls.Add(this.CheckedCountLabel);
            this.SearchGroupBox.Location = new System.Drawing.Point(12, 204);
            this.SearchGroupBox.Name = "SearchGroupBox";
            this.SearchGroupBox.Size = new System.Drawing.Size(400, 211);
            this.SearchGroupBox.TabIndex = 21;
            this.SearchGroupBox.TabStop = false;
            this.SearchGroupBox.Text = "Поиск";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 433);
            this.Controls.Add(this.ExpandCheckBox);
            this.Controls.Add(this.TreeViewLabel);
            this.Controls.Add(this.NumFindCountLabel);
            this.Controls.Add(this.FindCountLabel);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.TimeLabel);
            this.Controls.Add(this.SearchTimeLabel);
            this.Controls.Add(this.NumCheckedCountLabel);
            this.Controls.Add(this.CurFileNameLabel);
            this.Controls.Add(this.CurFileLabel);
            this.Controls.Add(this.criteriaGroupBox);
            this.Controls.Add(this.SearchGroupBox);
            this.Name = "Form1";
            this.Text = "TestApp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.criteriaGroupBox.ResumeLayout(false);
            this.criteriaGroupBox.PerformLayout();
            this.SearchGroupBox.ResumeLayout(false);
            this.SearchGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog DirBrowserDialog;
        private System.Windows.Forms.Button SetDirButton;
        private System.Windows.Forms.Label PathToDirLabel;
        private System.Windows.Forms.Label TempNameLabel;
        private System.Windows.Forms.TextBox TempNameTextBox;
        private System.Windows.Forms.Label FindTextLabel;
        private System.Windows.Forms.TextBox FindTextBox;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Label CurFileLabel;
        private System.Windows.Forms.Label CurFileNameLabel;
        private System.Windows.Forms.Label CheckedCountLabel;
        private System.Windows.Forms.Label NumCheckedCountLabel;
        private System.Windows.Forms.Label SearchTimeLabel;
        private System.Windows.Forms.Label TimeLabel;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.Timer timerSearch;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.GroupBox criteriaGroupBox;
        private System.Windows.Forms.Label FindCountLabel;
        private System.Windows.Forms.Label NumFindCountLabel;
        private System.Windows.Forms.Label TreeViewLabel;
        private System.Windows.Forms.CheckBox ExpandCheckBox;
        private System.Windows.Forms.GroupBox SearchGroupBox;
    }
}

