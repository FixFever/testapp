﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace TestApp
{
   public class Settings
    {
        public DirectoryInfo dir;
        public Regex regMask;
        public Regex regText;
        public Settings (DirectoryInfo d, Regex mask, Regex text)
        {
            dir = d;
            regMask = mask;
            regText = text;
        }
    }
}
